Gitlab-Runner
=========

This role sets up a private GitLab runner.

Requirements
------------

None.

Role Variables
--------------

runner_name: Name to give the runner in GitLab.

gitlab_url: URL of the GitLab instance to tie the runner to.

executor: Type of executor for the runner to use.

gitlab_token: Token for GitLab authentication.

Dependencies
------------

None.

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables
passed in as parameters) is always nice for users too:

```
---
- hosts: runners
  remote_user: ansible
  become: true

  roles:
    - { role: ansible-gitlab-runner, gitlab_token: "12345" }
```

License
-------

BSD

Author Information
------------------

John Hooks