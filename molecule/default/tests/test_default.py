import os

import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_runner_file(host):
    file = host.file('/etc/gitlab-runner/config.toml')
    assert file.exists
    assert file.contains('12345')
    assert file.contains('https://gitlab.com')
    assert file.contains('shell')


def test_runner_installed(host):
    runner = host.package("gitlab-runner")
    assert runner.is_installed


def test_runner_running(host):
    runner_service = host.service("gitlab-runner")
    assert runner_service.is_running
    assert runner_service.is_enabled
